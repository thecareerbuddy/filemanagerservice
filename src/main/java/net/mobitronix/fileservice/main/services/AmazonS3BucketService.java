package net.mobitronix.fileservice.main.services;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

@Service
public class AmazonS3BucketService {
	
	  private AmazonS3 amazonS3;

	    @Value("${endpointUrl}")
	    private String endpointUrl;
	    
	    @Value("${bucketName}")
	    private String bucketName;
	    
	    @Value("${accessKey}")
	    private String accessKey;
	    
	    @Value("${secretKey}")
	    private String secretKey;

	    @PostConstruct
	    private void initializeAmazon() {
	        AWSCredentials credentials = new BasicAWSCredentials(this.accessKey, this.secretKey);
	        this.amazonS3 = new AmazonS3Client(credentials);
	    }

	    public String uploadFile(MultipartFile multipartFile) {
	    	
	        String fileURL = "";
	        
	        try {
	        	
	            File file = convertMultipartFileToFile(multipartFile);
	            
	            String fileName = generateFileName(multipartFile);
	            
	            fileURL = endpointUrl + "/" + bucketName + "/images/" + fileName;
	            
	            uploadFileToBucket(fileName, file);
	            
	            file.delete();
	            
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        
	        return fileURL;
	    }

	    private File convertMultipartFileToFile(MultipartFile file) throws IOException {
	    	
	        File convertedFile = new File(file.getOriginalFilename());
	        FileOutputStream fos = new FileOutputStream(convertedFile);
	        fos.write(file.getBytes());
	        fos.close();
	        
	        return convertedFile;
	    }
	    
	    private String generateFileName(MultipartFile multiPart) {
	        return new Date().getTime() + "-" + multiPart.getOriginalFilename().replace(" ", "_");
	    }

	    private void uploadFileToBucket(String fileName, File file) {
	        amazonS3.putObject(new PutObjectRequest(bucketName+"/images", fileName, file)
	                .withCannedAcl(CannedAccessControlList.PublicRead));
	    }

	    public String deleteFileFromBucket(String fileName) {
	        amazonS3.deleteObject(new DeleteObjectRequest(bucketName, fileName));
	        return "Deletion Successful";
	    }


}
