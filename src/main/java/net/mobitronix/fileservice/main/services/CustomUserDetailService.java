package net.mobitronix.fileservice.main.services;


import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import net.mobitronix.fileservice.models.User;

@Service
public interface CustomUserDetailService extends UserDetailsService {

    public final String USER_SEQ_KEY = "user_seq_key";

    public abstract User createUser(User user);

    public abstract User editUser(User user);

    public abstract void deleteAllUsers();

    public abstract User findByEmail(String email);

    public abstract User findByUsernameOrPhoneNumber(String usernameOrPhoneNumber);

}
