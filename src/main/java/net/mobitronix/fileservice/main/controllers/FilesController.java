package net.mobitronix.fileservice.main.controllers;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.mobitronix.fileservice.main.services.AmazonS3BucketService;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class FilesController {
	
	
	private AmazonS3BucketService amazonS3BucketService;

	@Autowired
	FilesController(AmazonS3BucketService amazonS3BucketService) {
        this.amazonS3BucketService = amazonS3BucketService;
    }

    @PostMapping("/v1/storage/addFile")
    @ApiOperation(value = "Upload Files", notes = "Upload pictures and files to CareerBuddy's cloud storage")
    public String uploadFile(@RequestPart(value = "file") MultipartFile file) {
        return this.amazonS3BucketService.uploadFile(file);
    }

    @PostMapping("/v1/storage/deleteFile")
    @ApiOperation(value = "Upload Files", notes = "Remove pictures and files to CareerBuddy's cloud storage")
    public String deleteFile(@RequestBody String fileURL) {
        return this.amazonS3BucketService.deleteFileFromBucket(fileURL);
    }
}
