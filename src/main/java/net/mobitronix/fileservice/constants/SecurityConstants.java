package net.mobitronix.fileservice.constants;

public class SecurityConstants {

    public static final String AMAZON_S3_BUCKET_FILE_UPLOAD_PATH = "/api/v1/storage/addFile";
    public static final String AMAZON_S3_BUCKET_FILE_DELETE_PATH = "/api/v1/storage/deleteFile";

}
