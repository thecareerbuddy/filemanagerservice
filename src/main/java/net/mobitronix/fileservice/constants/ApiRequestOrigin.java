package net.mobitronix.fileservice.constants;

public enum ApiRequestOrigin {

    NONE, MERCH_MOBILE_APP, CLASSIFIED_WEB_APP, BILLING_PORTAL
}
