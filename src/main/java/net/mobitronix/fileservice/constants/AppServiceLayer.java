package net.mobitronix.fileservice.constants;

public enum AppServiceLayer {
    NONE, APP_SERVICE_IMPL, APP_CONTROLLER
}
