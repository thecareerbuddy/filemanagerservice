package net.mobitronix.fileservice.constants;

public enum MessageType {

    NONE, SMS, MMS, SIMPLE_EMAIL, TEMPLATED_EMAIL, BROKER_SERVICE_MSSG
}
