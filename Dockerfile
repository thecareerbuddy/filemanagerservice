FROM openjdk:8-jdk-alpine
VOLUME /tmp

COPY target/*.jar FileService-1.0.0.jar

ENTRYPOINT ["java","-jar","/FileService-1.0.0.jar"]